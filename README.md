### Nut House Escape Room VR

A student project at the **University of Applied Sciences Emden - Leer**.
Course "Grundlage Virtueller Welten" Summer 2020 - Prof. Thies Pfeiffer

## INSTRUCTIONS
---------------

# General:
* The project has been developed in **Unity version 2019.3.6f1** with the **SteamVR plugin** (through Asset Manager) using a HTC VIVE HMD for testing. 
* It was also tested on Oculus Quest 2 and Valve Index.

# Build:
* A Windows executable can be found in the **build** directory of the project.

# Unity:
To import the project into Unity you need to import it via the package provided in the **package** folder.

To do this follow these steps:
* Create a new Unity 3D Project for Unity version **2019.3.6f1**.
* Import the package from the **package** folder via **Assets** -> **Import Package** -> **Custom Package**.
* Grab a tea or coffee, this might take a while.
* If some HTC or SteamVR messages pop up just always press **Accept All**.
* When everything as been imported click the **MainMenu** scene from the Assets folder and wait until it has loaded.
* Add the **MainMenu** scene to the **Scenes In Build** list under **File** -> **Build Settings** -> **Add Open Scenes**
* Finally, save the scene and do the same with the **MainScene** scene.

You can build the project using Unity for target platform Windows (x86_64).rectly.

## PROJECT INFO:
---------------

* The start scene **MainMenu** and the main scene **MainScene** can be found directly in the Assets folder.
* Most other own Assets are stored in the project folder **NutHouseEscape**.
* If everything has been imported correctly all assets and packages used in this project should be there.
* The built-in Render Pipeline is used so don't try to use any fancy HDRP Shader Graph stuff unless you know what you're doing.

**Used packages:** 
* Oculus Desktop 2.38.4
* OpenVR Desktop 2.0.5
* Post Processing 2.3.0

**Assets used:**
* opengameart.org - Background music by Sergey_Cheremisinov
* Models 
   - books, pen, hammer, chest -> Sketchfab
   - lighter --> Asset Store

**Special Thanks to Tobit.Labs for sponsoring a HTC Vive :)**

## TROUBLESHOOT
---------------

**Crash on loading the *MainMenu* or *MainScene* **: 
* Can be related to the **Lighting Settings**.
* Turn Auto Generate off there and check if you need to switch to **Progressive CPU** rendering instead of **Progressive GPU** in the **Lightmapping Settings**